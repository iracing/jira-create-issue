# Bitbucket Pipelines Pipe: Jira Create Issue

With this pipe you can create a Jira issue from a Bitbucket Pipeline

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: atlassian/jira-create-issue:0.2.0
    variables:
      JIRA_BASE_URL: "<string>"
      JIRA_USER_EMAIL: "<string>"
      JIRA_API_TOKEN: "<string>"
      JIRA_PROJECT: "<string>"
      JIRA_ISSUE_SUMMARY: "<string>"
      # JIRA_ISSUE_TYPE: "<string>" # Optional
      # JIRA_ISSUE_DESCRIPTION: "<string>" # Optional
      # DEBUG: "<boolean>" # Optional
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| JIRA_BASE_URL (**)    | URL of Jira instance. Example: https://<yourdomain>.atlassian.net
| JIRA_USER_EMAIL (**)  | Email of the user for which Access Token was created for . Example: human@example.com
| JIRA_API_TOKEN (**)   | Access Token for Authorization. Example: HXe8DGg1iJd2AopzyxkFB7F2 (How To)
| JIRA_PROJECT (*)      | Jira project key
| JIRA_ISSUE_SUMMARY (*)| Issue summary
| JIRA_ISSUE_TYPE       | Issue type. Default: `Task` (minimum issue type used in various Jira default projects (eg. core, software classic, software nextgen)
| JIRA_ISSUE_DESCRIPTION | Issue details. Default: `Issue created from https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}` |
| JIRA_ISSUE_FIRST_COMMENT | Post an initial comment to the ticket with more information |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

(*) = required variable. This variable needs to be specified always when using the pipe.	
(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe.

## Prerequisites

To use this pipe you have to provide an API Access Token. You can follow the instructions [here](https://confluence.atlassian.com/cloud/api-tokens-938839638.html) to create one.

## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/jira-create-issue:0.2.0
    variables:
      JIRA_BASE_URL: $JIRA_BASE_URL
      JIRA_USER_EMAIL: $JIRA_USER_EMAIL
      JIRA_API_TOKEN: $JIRA_API_TOKEN
      JIRA_PROJECT: "PROJ"
      JIRA_ISSUE_SUMMARY: "This is a sample issue summary"
```

Advanced example:

The `JIRA_ISSUE_DESCRIPTION` field allows the customization of the resulting Jira issue's description. Unless specified with `JIRA_ISSUE_DESCRIPTION_SUFFIX=False` there will always be a link back to the pipeline that created the ticket (eg. `"Issue created from https://bitbucket.org/{repo_owner}/{repo_slug}/addon/pipelines/home#!/results/{build_bumber}"`). In customizing this description there is support for string replacements to get similar functionality by including `{repo_owner}`, `{repo_slug}`, and/or `{build_number}` which will help to build links to the resulting pipeline or repository if desired.

```yaml
script:
  - pipe: atlassian/jira-create-issue:0.2.0
    variables:
      JIRA_BASE_URL: $JIRA_BASE_URL
      JIRA_USER_EMAIL: $JIRA_USER_EMAIL
      JIRA_API_TOKEN: $JIRA_API_TOKEN
      JIRA_PROJECT: "PROJ"
      JIRA_ISSUE_SUMMARY: "This is a sample issue summary"
      JIRA_ISSUE_TYPE: "Story"
      JIRA_ISSUE_DESCRIPTION: "This is a sample issue summary created from {repo_owner}/{repo_slug} and from the CI/CD build related here, https://bitbucket.org/{repo_owner}/{repo_slug}/addon/pipelines/home#!/results/{build_number}"
      JIRA_ISSUE_FIRST_COMMENT: "This is some content to put in the first comment."
      JIRA_ISSUE_DESCRIPTION_SUFFIX: False
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community]([community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,jira).

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## Contributions

Contributions to Jira Create Issue are welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details. 

## License

Copyright (c) [2020] Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

<br/>

[![With â¤ï¸ from Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-cheers.png)](https://www.atlassian.com)