# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.0

- minor: Bump bitbucket-pipes-toolkit->2.2.0.

## 0.1.2

- patch: Internal maintenance: use absolute paths in docker image.

## 0.1.1

- patch: Internal maintenance: fix Dockerhub workspace.

## 0.1.0

- minor: Initial release of jira-create-issue pipe

