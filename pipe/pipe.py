import os

from bitbucket_pipes_toolkit import Pipe, get_logger, get_current_pipeline_url

import jira

schema = {
    'JIRA_BASE_URL': {'type': 'string', 'required': True},
    'JIRA_USER_EMAIL': {'type': 'string', 'required': True},
    'JIRA_API_TOKEN': {'type': 'string', 'required': True},
    'JIRA_PROJECT': {'type': 'string', 'required': True},
    'JIRA_ISSUE_SUMMARY': {'type': 'string', 'required': True},
    'JIRA_ISSUE_TYPE': {'type': 'string', 'required': False, 'default': 'Task'},
    'JIRA_ISSUE_DESCRIPTION': {'type': 'string', 'required': False, 'default': ""},
    'JIRA_ISSUE_DESCRIPTION_SUFFIX': {'type': 'boolean', 'required': False, 'default': True},
    'JIRA_ISSUE_FIRST_COMMENT': {'type': 'string', 'required': False, 'default': None},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class JiraCreateIssuePipe(Pipe):
    def run(self):
        super().run()

        self.log_info('Executing the pipe...')
        jira_url = self.get_variable('JIRA_BASE_URL')

        email = self.get_variable('JIRA_USER_EMAIL')
        api_token = self.get_variable('JIRA_API_TOKEN')

        project = self.get_variable('JIRA_PROJECT')
        summary = self.get_variable('JIRA_ISSUE_SUMMARY')
        issue_type = self.get_variable('JIRA_ISSUE_TYPE')

        first_comment = self.get_variable('JIRA_ISSUE_FIRST_COMMENT')

        description_template = self.get_variable('JIRA_ISSUE_DESCRIPTION')
        description = self.format_description(description_template)

        self.log_info(f'formatted description {description}')

        self.log_info('Initializing a Jira client')
        try:
            j = jira.JIRA(jira_url, basic_auth=(
                email, api_token), max_retries=0)
        except jira.JIRAError as e:
            self.log_error(e.text)
            self.fail(
                message=f'Failed to initialize a Jira client for {jira_url}')

        self.log_info(f'Creating a new issue in the project: {project}')

        try:
            new_issue = j.create_issue(project=project,
                                       summary=summary,
                                       issuetype={'name': issue_type},
                                       description=description)
        except jira.JIRAError as e:
            self.log_error(e.text)
            self.fail(message='Failed to create an issue')

        if first_comment:
            try:
                j.add_comment(new_issue.raw['id'], body=first_comment)
            except jira.JIRAError as e:
                self.log_error(e.text)
                self.fail(message='Failed to comment on issue')

        self.success(
            message=f'Successfully created a Jira issue. Follow this link to view the issue {new_issue.permalink()}')

    def format_description(self, template):
        description = template

        repo_owner = os.getenv('BITBUCKET_REPO_OWNER')
        repo_slug = os.getenv('BITBUCKET_REPO_SLUG')
        build_number = os.getenv('BITBUCKET_BUILD_NUMBER')

        if "{repo_owner}" in template:
            description = description.replace("{repo_owner}", repo_owner)
        if "{repo_slug}" in template:
            description = description.replace("{repo_slug}", repo_slug)
        if "{build_number}" in template:
            description = description.replace("{build_number}", build_number)

        suffix = self.get_variable('JIRA_ISSUE_DESCRIPTION_SUFFIX')
        if suffix:
            suffix_text = "Issue created from {}".format(get_current_pipeline_url())
            return '{}\n{}'.format(description, suffix_text)
        else:
            return description


if __name__ == '__main__':
    pipe = JiraCreateIssuePipe(pipe_metadata_file='/pipe.yml', schema=schema, check_for_newer_version=True)
    pipe.run()
