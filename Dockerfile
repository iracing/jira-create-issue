FROM python:3.8-slim

# install requirements
COPY requirements.txt /
WORKDIR /
RUN pip install --no-cache-dir -r requirements.txt

# copy the pipe source code
COPY LICENSE pipe.yml README.md /
COPY pipe /

ENTRYPOINT ["python3", "/pipe.py"]
