import os
import subprocess

docker_image = 'bitbucketpipelines/jira-create-issue:ci' + \
    os.getenv('BITBUCKET_BUILD_NUMBER', 'local')

# setup base run args with minimum default bitbucket environment variable mapping
base_run_args = [
    'docker',
    'run',
    '-e', f'BITBUCKET_REPO_OWNER={os.getenv("BITBUCKET_REPO_OWNER", "local")}',
    '-e', f'BITBUCKET_REPO_SLUG={os.getenv("BITBUCKET_REPO_SLUG", "local")}',
    '-e', f'BITBUCKET_BUILD_NUMBER={os.getenv("BITBUCKET_BUILD_NUMBER", 1234)}',
]


def docker_build():
    """
    Build the docker image for tests.
    :return:
    """
    args = [
        'docker',
        'build',
        '-t',
        docker_image,
        '.',
    ]
    subprocess.run(args, check=True)


def setup():
    docker_build()


def test_no_parameters():
    args = base_run_args + [
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 1
    assert '✖ Validation errors:' in result.stdout
    assert 'JIRA_USER_EMAIL:\n- required field' in result.stdout
    assert 'JIRA_API_TOKEN:\n- required field' in result.stdout
    assert 'JIRA_ISSUE_SUMMARY:\n- required field' in result.stdout
    assert 'JIRA_PROJECT:\n- required field' in result.stdout


def test_success_jira_core():
    args = base_run_args + [
        '-e', f'JIRA_BASE_URL={os.getenv("JIRA_BASE_URL")}',
        '-e', f'JIRA_USER_EMAIL={os.getenv("JIRA_USER_EMAIL")}',
        '-e', f'JIRA_API_TOKEN={os.getenv("JIRA_API_TOKEN")}',
        '-e', 'JIRA_PROJECT=CORE',
        '-e', 'JIRA_ISSUE_SUMMARY=Tests are passing',
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 0
    assert 'Successfully created' in result.stdout


def test_success_custom_jira_description():
    args = base_run_args + [
        '-e', f'JIRA_BASE_URL={os.getenv("JIRA_BASE_URL")}',
        '-e', f'JIRA_USER_EMAIL={os.getenv("JIRA_USER_EMAIL")}',
        '-e', f'JIRA_API_TOKEN={os.getenv("JIRA_API_TOKEN")}',
        '-e', 'JIRA_PROJECT=CORE',
        '-e', 'JIRA_ISSUE_SUMMARY=Tests are passing',
        '-e', 'JIRA_ISSUE_DESCRIPTION=custom description field',
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 0
    assert 'Successfully created' in result.stdout


def test_success_jira_first_comment():
    args = base_run_args + [
        '-e', f'JIRA_BASE_URL={os.getenv("JIRA_BASE_URL")}',
        '-e', f'JIRA_USER_EMAIL={os.getenv("JIRA_USER_EMAIL")}',
        '-e', f'JIRA_API_TOKEN={os.getenv("JIRA_API_TOKEN")}',
        '-e', 'JIRA_PROJECT=CORE',
        '-e', 'JIRA_ISSUE_SUMMARY=Tests are passing',
        '-e', 'JIRA_ISSUE_DESCRIPTION=custom description field',
        '-e', 'JIRA_ISSUE_FIRST_COMMENT=first comment field',
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 0
    assert 'Successfully created' in result.stdout


def test_success_custom_jira_description_no_suffix():
    args = base_run_args + [
        '-e', f'JIRA_BASE_URL={os.getenv("JIRA_BASE_URL")}',
        '-e', f'JIRA_USER_EMAIL={os.getenv("JIRA_USER_EMAIL")}',
        '-e', f'JIRA_API_TOKEN={os.getenv("JIRA_API_TOKEN")}',
        '-e', 'JIRA_PROJECT=CORE',
        '-e', 'JIRA_ISSUE_SUMMARY=Tests are passing',
        '-e', 'JIRA_ISSUE_DESCRIPTION=custom description field',
        '-e', 'JIRA_ISSUE_DESCRIPTION_SUFFIX=False',
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 0
    assert 'Successfully created' in result.stdout


def test_success_jira_software_nextgen():
    args = base_run_args + [
        '-e', f'JIRA_BASE_URL={os.getenv("JIRA_BASE_URL")}',
        '-e', f'JIRA_USER_EMAIL={os.getenv("JIRA_USER_EMAIL")}',
        '-e', f'JIRA_API_TOKEN={os.getenv("JIRA_API_TOKEN")}',
        '-e', 'JIRA_PROJECT=NEXTGEN',
        '-e', 'JIRA_ISSUE_TYPE=Task',
        '-e', 'JIRA_ISSUE_SUMMARY=Tests are passing',
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 0
    assert 'Successfully created' in result.stdout


def test_success_jira_software_classic():
    args = base_run_args + [
        '-e', f'JIRA_BASE_URL={os.getenv("JIRA_BASE_URL")}',
        '-e', f'JIRA_USER_EMAIL={os.getenv("JIRA_USER_EMAIL")}',
        '-e', f'JIRA_API_TOKEN={os.getenv("JIRA_API_TOKEN")}',
        '-e', 'JIRA_PROJECT=CLASSIC',
        '-e', 'JIRA_ISSUE_TYPE=Story',
        '-e', 'JIRA_ISSUE_SUMMARY=Tests are passing',
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 0
    assert 'Successfully created' in result.stdout


def test_fail_wrong_token():
    args = base_run_args + [
        '-e', f'JIRA_BASE_URL={os.getenv("JIRA_BASE_URL")}',
        '-e', f'JIRA_USER_EMAIL={os.getenv("JIRA_USER_EMAIL")}',
        '-e', f'JIRA_API_TOKEN=youshallnotpass',
        '-e', 'JIRA_PROJECT=PIPE',
        '-e', 'JIRA_ISSUE_SUMMARY=Tests are passing',
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 1
    assert 'Failed to initialize' in result.stdout


def test_fail_no_project():
    args = base_run_args + [
        '-e', f'JIRA_BASE_URL={os.getenv("JIRA_BASE_URL")}',
        '-e', f'JIRA_USER_EMAIL={os.getenv("JIRA_USER_EMAIL")}',
        '-e', f'JIRA_API_TOKEN={os.getenv("JIRA_API_TOKEN")}',
        '-e', 'JIRA_PROJECT=DNE',
        '-e', 'JIRA_ISSUE_SUMMARY=Tests are passing',
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 1
    assert "No project could be found with key 'DNE'" in result.stderr
    assert 'Failed to create an issue' in result.stdout
